package com.arima.mygymuser.auth;

import android.app.Activity;

import com.arima.mygymuser.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class FirebaseAuthManager {
    private FirebaseAuth auth;

    public FirebaseAuthManager() {
        auth = FirebaseAuth.getInstance();
    }

    public boolean isLogin() {
        return auth.getCurrentUser() != null;
    }

    public void login(Activity context, String email, String password, OnCompleteListener<AuthResult> responseHandler) {
        if(email.isEmpty() || password.isEmpty()) {
            return;
        }
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(context, responseHandler);
    }

    public User getUser() {
        if(!isLogin()) {
            return null;
        }
        User user = new User();
        user.setId(auth.getCurrentUser().getUid());
        user.setEmail(auth.getCurrentUser().getEmail());
        return user;
    }

    public void logout() {
        this.auth.signOut();
    }

    public void createAccount(String email, String password, OnCompleteListener<AuthResult> onComplete) {
        if(isLogin()) return;

        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(onComplete);
    }
}
