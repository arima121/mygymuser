package com.arima.mygymuser.model;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.PropertyName;

import java.util.ArrayList;

public class User {
    private String id;
    private String name;
    private String email;
    private String address;
    private String dni;
    private String phone;
    private int accountType;
    private ArrayList<Target> targets;
    private Target defaultTargetAdmin;
    private Target defaultTargetUser;

    public static final int USER_TYPE_ADMIN = 100;
    public static final int USER_TYPE_USER = 1;

    public User() {
        targets = new ArrayList<>();
    }

    @Exclude
    public String getId() {
        return id;
    }

    @Exclude
    public void setId(String id) {
        this.id = id;
    }

    @PropertyName("name")
    public String getName() {
        return name;
    }

    @PropertyName("name")
    public void setName(String name) {
        this.name = name;
    }

    @PropertyName("email")
    public String getEmail() {
        return email;
    }

    @PropertyName("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @PropertyName("address")
    public String getAddress() {
        return address;
    }

    @PropertyName("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @PropertyName("dni")
    public String getDni() {
        return dni;
    }

    @PropertyName("dni")
    public void setDni(String dni) {
        this.dni = dni;
    }

    @PropertyName("phone")
    public String getPhone() {
        return phone;
    }

    @PropertyName("phone")
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @PropertyName("accountType")
    public int getAccountType() {
        return accountType;
    }

    @PropertyName("accountType")
    public void setAccountType(int accountType) {
        this.accountType = accountType;
    }

    @PropertyName("targets")
    public ArrayList<Target> getTargets() {
        return targets;
    }

    @PropertyName("targets")
    public void setTargets(ArrayList<Target> targets) {
        this.targets = targets;
    }

    @PropertyName("defaultTargetAdmin")
    public Target getDefaultTargetAdmin() {
        return defaultTargetAdmin;
    }

    @PropertyName("defaultTargetAdmin")
    public void setDefaultTargetAdmin(Target defaultTargetAdmin) {
        this.defaultTargetAdmin = defaultTargetAdmin;
    }

    @PropertyName("defaultTargetUser")
    public Target getDefaultTargetUser() {
        return defaultTargetUser;
    }

    @PropertyName("defaultTargetUser")
    public void setDefaultTargetUser(Target defaultTargetUser) {
        this.defaultTargetUser = defaultTargetUser;
    }

    public void addTarget(Target newTarget) {
        for(Target target: targets) {
            if(target.getId().equals(newTarget.getId())) {
                target.setType(newTarget.getType());
                return;
            }
        }

        targets.add(newTarget);
    }
}
