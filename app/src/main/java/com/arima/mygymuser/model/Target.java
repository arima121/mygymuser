package com.arima.mygymuser.model;

import com.google.firebase.firestore.PropertyName;

public class Target {
    private String id;
    private int type;

    public static final int TARGET_TYPE_ADMIN = 100;
    public static final int TARGET_TYPE_MANAGER = 50;
    public static final int TARGET_TYPE_USER = 1;

    @PropertyName("id")
    public String getId() {
        return id;
    }

    @PropertyName("id")
    public void setId(String id) {
        this.id = id;
    }

    @PropertyName("type")
    public int getType() {
        return type;
    }

    @PropertyName("type")
    public void setType(int type) {
        this.type = type;
    }
}
