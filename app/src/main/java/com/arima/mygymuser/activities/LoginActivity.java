package com.arima.mygymuser.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.arima.mygymuser.R;
import com.arima.mygymuser.auth.FirebaseAuthManager;
import com.arima.mygymuser.model.Gym;
import com.arima.mygymuser.model.Target;
import com.arima.mygymuser.model.User;
import com.arima.mygymuser.storage.FirestoreManager;
import com.arima.mygymuser.storage.SharedPreferencesManager;
import com.google.android.material.snackbar.Snackbar;

public class LoginActivity extends AppCompatActivity {
    private LoginActivity thisActivity;

    private FirebaseAuthManager authManager;
    private FirestoreManager dbManager;
    private SharedPreferencesManager preferencesManager;

    private EditText email;
    private EditText password;
    private Button login;
    private Button create;
    private ProgressBar loadingPB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        createInternalItems();

        createViewItems();
    }

    public void createInternalItems() {
        thisActivity = this;

        authManager = new FirebaseAuthManager();
        dbManager = new FirestoreManager();
        preferencesManager = new SharedPreferencesManager(thisActivity);
    }

    public void createViewItems() {
        email = findViewById(R.id.login_email);
        password = findViewById(R.id.login_password);
        login = findViewById(R.id.login_ok);
        create = findViewById(R.id.login_create);
        loadingPB = findViewById(R.id.loading);
    }

    public boolean validate() {
        boolean valid = true;
        if(email.getText().toString().isEmpty()) {
            valid = false;
            email.setError(getString(R.string.login_error_no_email));
        }
        if(password.getText().toString().isEmpty()) {
            valid = false;
            password.setError(getString(R.string.login_error_no_password));
        }
        return valid;
    }

    public void login(View view) {
        if(!validate()) return;

        setLoadingState(true);

        authManager.login(thisActivity, email.getText().toString(), password.getText().toString(), task -> {
            if(!task.isSuccessful()) {
                authManager.logout();
                Snackbar.make(view, R.string.login_error, Snackbar.LENGTH_LONG).show();
                setLoadingState(false);
                return;
            }
            setLoadingState(false);
            validateUser();
        });
    }

    public void validateUser() {
        User currentUser = authManager.getUser();
        dbManager.getDocument(FirestoreManager.FS_COLLECTION_USERS, currentUser.getId(), response -> {
            if(!response.isSuccessful()) {
                authManager.logout();
                Snackbar.make(email, R.string.login_no_user_error, Snackbar.LENGTH_LONG).show();
                setLoadingState(false);
                return;
            }

            if(response.getResult() == null) {
                authManager.logout();
                Snackbar.make(email, R.string.login_no_user_error, Snackbar.LENGTH_LONG).show();
                setLoadingState(false);
                return;
            }

            User responseUser = response.getResult().toObject(User.class);

            responseUser.setId(currentUser.getId());
            responseUser.setEmail(currentUser.getEmail());

            if(responseUser.getDefaultTargetUser() == null) {
                setLoadingState(false);
                goToClientList();
                return;
            }

            preferencesManager.savePreference(SharedPreferencesManager.SP_USER, responseUser);

            loadDefaultGym(responseUser.getDefaultTargetUser());
        });
    }

    private void loadDefaultGym(Target target) {
        if(target == null) {
            setLoadingState(false);
            goToClientList();
            return;
        }
        dbManager.getDocument(FirestoreManager.FS_COLLECTION_GYMS, target.getId(), response -> {
            if(response.isSuccessful() && response.getResult() != null) {
                Gym defaultGym = response.getResult().toObject(Gym.class);
                defaultGym.setId(response.getResult().getId());
                preferencesManager.savePreference(SharedPreferencesManager.SP_GYM, defaultGym);
            }
            setLoadingState(false);
            goToClientList();
        });
    }

    public void setLoadingState(boolean loading) {
        email.setEnabled(!loading);
        password.setEnabled(!loading);
        login.setEnabled(!loading);
        create.setEnabled(!loading);

        login.setClickable(!loading);
        create.setClickable(!loading);

        loadingPB.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    public void goToClientList(){
        Intent intent = new Intent(thisActivity, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void goToCreateAccount(View view) {
        Intent intent = new Intent(thisActivity, CreateAccountActivity.class);
        startActivity(intent);
    }
}
