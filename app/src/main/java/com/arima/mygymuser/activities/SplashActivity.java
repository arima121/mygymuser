package com.arima.mygymuser.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.arima.mygymuser.auth.FirebaseAuthManager;
import com.arima.mygymuser.model.Gym;
import com.arima.mygymuser.model.Target;
import com.arima.mygymuser.model.User;
import com.arima.mygymuser.storage.FirestoreManager;
import com.arima.mygymuser.storage.SharedPreferencesManager;

public class SplashActivity extends AppCompatActivity {
    private SplashActivity thisActivity;

    private FirebaseAuthManager authManager;
    private FirestoreManager dbManager;
    private SharedPreferencesManager preferencesManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        createInternalItems();

        checkUserAndRedirect();
    }

    public void createInternalItems() {
        thisActivity = this;

        authManager = new FirebaseAuthManager();
        dbManager = new FirestoreManager();
        preferencesManager = new SharedPreferencesManager(thisActivity);
    }

    private void checkUserAndRedirect() {
        if(!authManager.isLogin()) {
            invalidLogin(false);
            return;
        }
        User currentUser = authManager.getUser();
        dbManager.getDocument(FirestoreManager.FS_COLLECTION_USERS, currentUser.getId(), response -> {
            if(!response.isSuccessful()) {
                invalidLogin(true);
                return;
            }

            if(response.getResult() == null) {
                invalidLogin(true);
                return;
            }

            User responseUser = response.getResult().toObject(User.class);
            responseUser.setId(currentUser.getId());
            responseUser.setEmail(currentUser.getEmail());

            preferencesManager.savePreference(SharedPreferencesManager.SP_USER, responseUser);

            loadDefaultGym(responseUser.getDefaultTargetUser());
        });
    }

    private void loadDefaultGym(Target target) {
        if(target == null) {
            redirectToMainActivity();
            return;
        }

        dbManager.getDocument(FirestoreManager.FS_COLLECTION_GYMS, target.getId(), response -> {
            if(response.isSuccessful() && response.getResult() != null) {
                Gym defaultGym = response.getResult().toObject(Gym.class);
                defaultGym.setId(response.getResult().getId());
                preferencesManager.savePreference(SharedPreferencesManager.SP_GYM, defaultGym);
            }
            redirectToMainActivity();
        });
    }

    private void invalidLogin(boolean logout) {
        if(logout) {
            authManager.logout();
        }
        preferencesManager.clearSharedPreferences();
        redirectToLogin();
    }

    public void redirectToLogin() {
        Intent intent = new Intent(thisActivity, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public void redirectToMainActivity() {
        Intent intent = new Intent(thisActivity, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
