package com.arima.mygymuser.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.arima.mygymuser.R;
import com.arima.mygymuser.auth.FirebaseAuthManager;
import com.arima.mygymuser.model.User;
import com.arima.mygymuser.storage.FirestoreManager;
import com.arima.mygymuser.storage.SharedPreferencesManager;
import com.google.android.material.snackbar.Snackbar;

public class CreateAccountActivity extends AppCompatActivity {
    private CreateAccountActivity thisActivity;

    private FirebaseAuthManager authManager;
    private FirestoreManager dbManager;
    private SharedPreferencesManager preferencesManager;

    private EditText email;
    private EditText password;
    private EditText passwordConf;
    private Button create;
    private ProgressBar loadingPB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        createInternalItems();

        createViewItems();
    }

    public void createInternalItems() {
        thisActivity = this;

        authManager = new FirebaseAuthManager();
        dbManager = new FirestoreManager();
        preferencesManager = new SharedPreferencesManager(thisActivity);
    }

    public void createViewItems() {
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        passwordConf = findViewById(R.id.password_conf);
        create = findViewById(R.id.login_create);
        loadingPB = findViewById(R.id.loading);
    }

    public boolean validate() {
        boolean valid = true;
        if(email.getText().toString().isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
            valid = false;
            email.setError(getString(R.string.create_error_no_email));
        }
        if(password.getText().toString().isEmpty()) {
            valid = false;
            password.setError(getString(R.string.create_error_no_password));
        } else if(password.getText().toString().length() < 6) {
            valid = false;
            password.setError(getString(R.string.create_error_password_size));
        } else if(!passwordConf.getText().toString().equals(password.getText().toString())) {
            valid = false;
            passwordConf.setError(getString(R.string.create_error_passwords_mismatch));
        }
        if(passwordConf.getText().toString().isEmpty()) {
            valid = false;
            passwordConf.setError(getString(R.string.create_error_no_password_conf));
        }
        return valid;
    }

    public void createAccount(View view) {
        if(!validate()) return;
        setLoadingState(true);

        authManager.createAccount(
                email.getText().toString(),
                password.getText().toString(),
                result -> {
                    if(result != null && result.isSuccessful() && result.getResult() != null) {
                        User user = authManager.getUser();
                        user.setAccountType(User.USER_TYPE_USER);
                        createUserDBEntry(user);
                    } else {
                        setLoadingState(false);
                        Snackbar.make(email, getString(R.string.create_error_fail), Snackbar.LENGTH_LONG).show();
                    }
                });
    }

    public void createUserDBEntry(User user) {
        dbManager.setValue(
                FirestoreManager.FS_COLLECTION_USERS,
                user.getId(),
                user,
                success -> {
                    Snackbar.make(email, getString(R.string.create_success_created), Snackbar.LENGTH_LONG).show();
                    preferencesManager.savePreference(SharedPreferencesManager.SP_USER, user);
                    setLoadingState(false);
                    goToMainActivity();
                },
                fail -> {
                    setLoadingState(false);
                });
    }

    public void goToMainActivity() {
        Intent intent = new Intent(thisActivity, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void setLoadingState(boolean loading) {
        email.setEnabled(!loading);
        password.setEnabled(!loading);
        passwordConf.setEnabled(!loading);
        create.setEnabled(!loading);

        create.setClickable(!loading);

        loadingPB.setVisibility(loading ? View.VISIBLE : View.GONE);
    }
}
